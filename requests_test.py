import requests
import json
import pandas

url = "https://mapi.lfmall.co.kr/api/search/v2/categories"
data = json.dumps({
    "aggs":["saleType", "colors", "season", "styleYear", "brandGroup", "gender", "searchCategory", "price", "prop1", "prop2", "size1", "size2", "benefit", "review"],
    "order": "popular",
    "page" : 1,
    "pid": [80104],
    "size": 40,
    "tid": [80047]
    })
headers = {
    "Content-Type": "application/json;"
}

response = requests.post(url=url, data=data, headers=headers)

products = response.json()["results"]["products"]  
 
rows = []

for product in products:
    brand_name = product["brandEngName"]
    item_name = product["name"]
    price = product["salePrice"]
    rows.append([brand_name, item_name, price])
     
df = pandas.DataFrame(data=rows, columns=["brand_name", "item_name", "price"])
#print(rows)
df.to_excel("LFmall.xlsx")