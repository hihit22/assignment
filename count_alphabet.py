# 스크립트 이름: count_alphabet.py
# Input:
# 임의의 문자열
# Top N
# Output:
# 개수 순서대로 Top N개의 알파벳 글자와 개수를 출력
# 제약:
# 대소문자를 구별하지 않음
# 알파벳 이외의 문자는 무시
# 개수가 동일한 알파벳은 알파벳 순으로 정렬

import re
import argparse
from collections import Counter 
 
def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("item")
    parser.add_argument("n")
    return parser.parse_args()
 
 
def main():
    args = get_args()
    output = re.sub("[^a-zA-Z]", "", args.item)
    output = list(output)
    n = int(args.n)
    output1 =[]
    for i in output:
        output1.append(i.lower())

    output1.sort()
    print(Counter(output1).most_common(n))
    pass
 
 
if __name__ == "__main__":
    main()