import argparse
 
 
def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("item", nargs="+")
    parser.add_argument("--sorted", action = 'store_true')
    return parser.parse_args()
 
 
def main():
    args = get_args()
    output = args.item

    deduplication = set(output)
    output = list(deduplication)

    if args.sorted:
        output.sort()

    output_len = len(output)

    if output_len == 1:
        print(output[0])
    elif output_len == 2:
        print(output[0] +' and ' + output[1])
    elif output_len >= 3:
        print(', '.join(output[0:-1])+' and ' + output[-1])
    pass
 
 
if __name__ == "__main__":
    main()